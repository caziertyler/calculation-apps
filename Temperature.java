/**
 * This class converts farenheit to celsius.
 * 
 * @author  Tyler Cazier
 * @version January 11, 2013
 */

package assignment01;

import java.util.Scanner;

public class Temperature {

	public static void main(String[] args) {
		
		// Declares variables.

		double farenheit;
		double celsius;
		
		// Explains program.
		
		System.out.println ("This program will convert the temperature in farenheit to celsius.");
		 
        // Retrieves farenheit from the user.
		
		System.out.print ("Enter the temperature in farenheit: ");
		
		Scanner inputFarenheit;
		inputFarenheit = new Scanner(System.in);
		farenheit = inputFarenheit.nextDouble();
 
        // Calculates the degrees in celsius.
		
		
		celsius = (double)5 / (double)9 * (farenheit - 32);
 
        // Prints answer to screen.
		
		System.out.println (farenheit + " degrees farenheit is equal to " + celsius + " degrees celsius.");
	}

}
