/**
 * This class retrieves the two small sides of a triangle from the user and outputs the hypotenuse.
 * 
 * @author  Tyler Cazier
 * @version January 11, 2013
 */

package assignment01;

import java.util.Scanner;

public class Hypotenuse {

	public static void main(String[] args) {
		
		// Declares variables.
		
		int sideA;
		int sideB;
		double hypotenuse;
		
		// Explains program.
		
		System.out.println ("This program will calculate the hypotenuse of a right triangle based on the the lengths of the two smaller sides you provide.");
		 
        // Retrieves sideA from the user.
		
		System.out.print ("Enter the length of the first side of the triangle: ");
		
		Scanner inputSides;
		inputSides = new Scanner(System.in);
		sideA = inputSides.nextInt();
		
		// Retrieves sideB from the user.
		
		System.out.print ("Enter the length of the second side of the triangle: ");
		
		sideB = inputSides.nextInt();
 
        // Calculates the hypotenuse of the triangle.
		
		hypotenuse = Math.sqrt(Math.pow(sideA, 2) + Math.pow(sideB, 2));
 
        // Prints answer to screen.
		
		System.out.println ("The hypotenuse of the triangle is " + hypotenuse + ".");

	}

}
