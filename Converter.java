/**
 * This class converts inches to millimeters.
 * 
 * @author  Tyler Cazier
 * @version January 11, 2013
 */

package assignment01;

import java.util.Scanner;

public class Converter {

	public static void main(String[] args) {
		
		// Declares variables.
		
		int inches;
		double millimeters;
		
		// Explains program.
		
		System.out.println ("This program converts inches to millimeters.");
		 
        // Retrieves the amount of inches from the user.
		
		System.out.print ("Enter the amount of inches you would like to convert: ");
		
		Scanner inputInches;
		inputInches = new Scanner(System.in);
		inches = inputInches.nextInt();
 
        // Converts user's input to millimeters.
		
		millimeters = inches * 25.4;
 
        // Prints answer to screen.
		
		System.out.println (inches + " inches  is equal to " + millimeters + " millimeters.");

	}

}
