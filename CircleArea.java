/**
 * This class calculates the area of a circle based on the radius given by the user.
 * 
 * @author  Tyler Cazier
 * @version January 11, 2013
 */

package assignment01;

import java.util.Scanner;

public class CircleArea {

	public static void main(String[] args) {
		
		// Declares variables.
		
		int radius;
		double circleArea;
		
		// Explains program.
		
		System.out.println ("This program will calculate the area of a circle based on the radius you provide.");
		 
        // Retrieves the radius from the user.
		
		System.out.print ("Enter the radius of the circle: ");
		
		Scanner inputRadius;
		inputRadius = new Scanner(System.in);
		radius = inputRadius.nextInt();
 
        // Calculates the area of the circle.
		
		circleArea = Math.PI * Math.pow(radius, 2);
 
        // Prints answer to screen.
		
		System.out.println ("A circle with a radius of " + radius + " has an area of " + circleArea + ".");

	}

}
